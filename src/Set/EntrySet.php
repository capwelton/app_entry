<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Entry\Set;

/**
* A class used to store various text entries, usually
* for autocompletion of form fields.
*
* @property ORM_StringField    $category
* @property ORM_StringField    $text
* @property ORM_StringField    $description
* @property ORM_BoolField      $checked
*/
class EntrySet extends \app_TraceableRecordSet
{
    
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName($App->classPrefix.'Entry');
        
        $this->addFields(
            ORM_StringField('category')
            ->setDescription('Entry category'),
            ORM_StringField('text')
            ->setDescription('Entry text'),
            ORM_StringField('description')
            ->setDescription('Entry description'),
            ORM_BoolField('checked')
            ->setDescription('Checked')
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new EntryBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new EntryAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function isReadable()
    {
        //Default rights. The class implementing the component should override this method
        return $this->all();
    }
    
    public function isUpdatable()
    {
        //Default rights. The class implementing the component should override this method
        return $this->all();
    }
    
    public function isCreatable()
    {
        //Default rights. The class implementing the component should override this method
        return $this->all();
    }
    
    public function isDeletable()
    {
        //Default rights. The class implementing the component should override this method
        return $this->all();
    }
}

class EntryBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class EntryAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}