<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Entry\Ui;
use Capwelton\App\Entry\Set\EntrySet;

bab_Widgets()->includePhpClass('widget_TableView');


class EntryTableView extends \app_TableModelView
{

    public function addDefaultColumns(EntrySet $set)
    {
        $App =  $this->App();
        
        $this->addColumn(widget_TableModelViewColumn($set->text, $App->translate('Text'))->setExportable(true));
        $this->addColumn(widget_TableModelViewColumn($set->category, $App->translate('Category'))->setExportable(true));
        $this->addColumn(widget_TableModelViewColumn($set->description, $App->translate('Description'))->setExportable(true));
        $this->addColumn(widget_TableModelViewColumn($set->checked, $App->translate('Checked'))->setExportable(true));
    }
    
	protected function computeCellContent($record, $fieldPath)
	{
		$App = $record->App();
		$W = bab_Widgets();
		
		if ($fieldPath == '__actions__') {

			$cellContent = $W->Menu()
						->setLayout($W->FlowLayout())
						->addClass(\Func_Icons::ICON_LEFT_16);
			if (!$record->checked) {
				$cellContent->addItem(
					$W->Link($W->Icon($App->translate('Validate'), \Func_Icons::ACTIONS_DIALOG_OK), $App->Controller()->Admin()->validateEntry($record->id))
				);
			}
			$cellContent->addItem(
				$W->Link($W->Icon($App->translate('Edit...'), \Func_Icons::ACTIONS_DOCUMENT_EDIT), $App->Controller()->Admin()->editEntry($record->id))
			);
			$cellContent->addItem(
				$W->Link($W->Icon($App->translate('Delete'), \Func_Icons::ACTIONS_EDIT_DELETE), $App->Controller()->Admin()->deleteEntry($record->id))
					->setConfirmationMessage($App->translate('Are you sure you want to delete this entry?'))
			);
			
		} else if ($fieldPath == 'text') {
			$cellContent = $W->VBoxItems(
				$W->Link($record->text, $App->Controller()->Entry()->edit($record->id))
			);
			
		} else if ($fieldPath == 'modifiedOn') {
			$cellContent = $W->VBoxItems(
				$W->Label(bab_shortDate(bab_mktime($record->modifiedOn), true)),
				$W->Label(bab_getUserName($record->modifiedBy, true))->addClass('crm-sub-label')
			);
		} else {
			$cellContent = parent::computeCellContent($record, $fieldPath);
		}
		
		if ($record->checked) {
			$cellContent->addClass('checked');
		} else {
			$cellContent->addClass('not-checked');
		}
		
		return $cellContent;
		
	}
}



