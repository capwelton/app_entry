<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Entry\Ui;


class EntryUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * @return EntryTableView
     */
    public function tableView($id = null)
    {
        return $this->EntryTableView($id);
    }
    
    /**
     * @return EntryEditor
     */
    public function editor($id = null, \Widget_Layout $layout = null)
    {
        return $this->EntryEditor($id, $layout);
    }
    
    public function EntryTableView($id = null)
    {
        return new EntryTableView($this->app, $id);
    }
    
    public function EntryEditor($id = null, \Widget_Layout $layout = null)
    {
        return new EntryEditor($this->app, $id, $layout);
    }
}